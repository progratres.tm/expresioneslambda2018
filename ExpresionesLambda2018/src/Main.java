import java.util.HashMap;
import java.util.Map;
import java.util.function.IntFunction;

public class Main 
{
	public static void main(String[] args) 
	{
		// se implementa el m�todo de la interfaz con una expresi�n lambda
		IFuncional1 ifuncional1 = (a, b) -> System.out.println(a + b);

		// se utiliza el m�todo con la implementaci�n y se le env�a los valores
		// de x e y
		ifuncional1.suma(3, 2);
		
		IntFunction<String> funcion = new IntFunction<String>()
		{
			@Override
			public String apply(int value) 
			{
				String ret = "Devuelve ";
				return ret + value;
			}
		};
			
		System.out.println(funcion.apply(7));

		Map<String, Integer> salarios = new HashMap<>();
		salarios.put("Juan", 20000);
		salarios.put("Fede", 30000);
		salarios.put("Seba", 35000);
		System.out.println(salarios);
			
		salarios.replaceAll((nombre, viejoValor) -> 
		nombre.equals("Fede") ? viejoValor + 5000 : viejoValor + 10000);
		System.out.println(salarios);
	}
}
