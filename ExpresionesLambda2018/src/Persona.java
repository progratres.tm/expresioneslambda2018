public class Persona 
{
	private String nombre;
	private int edad;
	private int peso;
	
	public Persona (String n, int e, int p)
	{
		nombre = n;
		edad = e;
		peso = p;
	}

	public String getNombre() 
	{
		return nombre;
	}

	public int getEdad() 
	{
		return edad;
	}
	
	public int getPeso()
	{
		return peso;
	}
		
	public void mostrarNombre()
	{
		System.out.println(this.nombre);
	}
	
	public void mostrarEdad()
	{
		System.out.println(this.edad);
	}
	
	public Boolean esMayor()
	{
		return this.edad>=21;
	}
	
	public double mediaEdad()
	{
		return edad/2.0;
	}
	
	@Override
	public String toString() 
	{
		return "[nombre=" + nombre + ", edad=" + edad + ", peso=" + peso + "]";
	}
}
