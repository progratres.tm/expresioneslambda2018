import java.util.function.Consumer;

public class Consumidor 
{
	public static void main(String[] args) 
	{
		Consumer<Integer> elDoble = new Consumer<Integer>() 
		{
			@Override
			public void accept(Integer x) 
			{
				System.out.println(2 * x);
			}
		};

		System.out.println("Probando el doble");
		elDoble.accept(4);

		Consumer<Integer> elTriple = x -> System.out.println(3 * x);
		elTriple.accept(7);

		Consumer<String> consumidor = (x) -> System.out.println(x);
		consumidor.accept("hola");

		procesar(x -> System.out.println(3 * x), 4);
		procesar(x -> System.out.println(x + " " + x), "hola");
		procesar(x -> System.out.println(x.toUpperCase()), "pasame a mayusculas");	
		procesar(x -> imprimir(x), "hola");
	}

	public static <T> void procesar(Consumer<T> expresion, T mensaje) 
	{
		expresion.accept(mensaje);
	}

	public static void imprimir(String mensaje) 
	{
		System.out.println("---------");
		System.out.println(mensaje);
		System.out.println("---------");
	}
}
