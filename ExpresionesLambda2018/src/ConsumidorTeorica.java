import java.util.ArrayList;
import java.util.function.Consumer;

public class ConsumidorTeorica 
{
	public static void main(String[] args) 
	{
		ArrayList<Persona> personas = new ArrayList<Persona>() ;
		
		personas.add(new Persona("Ana",10, 50));
		personas.add(new Persona("Mia",11, 50));		
		personas.add(new Persona("Paula",12, 60));
		personas.add(new Persona("Manuel",28, 40));
		personas.add(new Persona("Sofia",22, 30));

		aplicar(personas, p -> p.mostrarNombre());		
		aplicar(personas, p -> p.mostrarEdad());
		
		//No necesitamos los metodos mostrarNombre, mostrarEdad, lo definimos dentro de la expresion lambda
		aplicar(personas, p -> { System.out.println(p.getNombre() ); });
		aplicar(personas, p -> { System.out.println(p.getEdad() ); });
		
		aplicar(personas, p -> { System.out.println(p.getNombre() + " " + p.getEdad()); });
		//Tampoco necesitamos las llaves si es una sola instruccion
		aplicar(personas, p -> System.out.println(p.getNombre() + " " + p.getEdad()));
		
		ArrayList<Integer> losEnteros= new ArrayList<Integer>();
	    losEnteros.add(6);
	    losEnteros.add(16);
	    losEnteros.add(76);
	    
	    System.out.println("con generics");
	    printArray(losEnteros, i -> System.out.print(" " + i));
	    System.out.println("");
	    printArray(personas, i -> System.out.print(" " + i));
	}

	public static void aplicar(ArrayList<Persona> personas, Consumer<Persona> consumidor)
	{
		for(Persona persona: personas)
			consumidor.accept(persona);
	}
	
	public static <T> void printArray(ArrayList<T> losEnteros, Consumer<T> consumer)
	{
		  for(T numero : losEnteros)
		    consumer.accept(numero);
	}
}
