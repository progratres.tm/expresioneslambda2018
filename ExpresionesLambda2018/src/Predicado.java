import java.util.ArrayList;
import java.util.function.Predicate;

public class Predicado 
{
	public static void main(String[] args) 
	{
		Predicate<String> predicado = new Predicate<String>() 
		{
			@Override
			public boolean test(String t) 
			{
				return  t.startsWith("S");
			}
		};
		
		System.out.println(predicado.test("Sabor"));		
		System.out.println(predicado.test("Color"));
		
		Predicate<String> comienzaConS = x -> x.startsWith("S");
		System.out.println(comienzaConS.test("Sabor"));
		
		ArrayList<Integer> losEnteros= new ArrayList<Integer>();
	    losEnteros.add(6);
	    losEnteros.add(-16);
	    losEnteros.add(75);
	    losEnteros.add(-91);
		
	    Predicate<Integer> positivo = i -> i > 0;
	    ArrayList<Integer> filtrados = filtro(losEnteros, positivo);
		System.out.println(filtrados);
		
		//Puedo pasar directo el predicado
		System.out.println(filtro(losEnteros, x -> x < 0));
		System.out.println(filtro(losEnteros, x -> x < -50 || x > 70));	
	}
	
	public static ArrayList<Integer> filtro(ArrayList<Integer> losEnteros, Predicate<Integer> predicate)
	{
		  ArrayList<Integer> filteredList = new ArrayList<Integer>();
		  for(Integer numero:losEnteros) if(predicate.test(numero))
			  filteredList.add(numero);
		  
		  return filteredList;
	}
}
