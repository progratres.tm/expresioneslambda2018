import java.util.ArrayList;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class PredicadoTeorica 
{
	public static void main(String[] args) 
	{
		ArrayList<Persona> personas = new ArrayList<Persona>() ;
		
		personas.add(new Persona("Ana",10, 50));
		personas.add(new Persona("Mia",11, 50));		
		personas.add(new Persona("Paula",12, 60));
		personas.add(new Persona("Manuel",28, 40));
		personas.add(new Persona("Sofia",22, 30));

		filtrar(personas, (p -> p.esMayor()));
		System.out.println("");
		filtrar(personas, p -> { return p.getEdad() > 11; });	

		System.out.println("");
		filtrar2(personas, p -> { return p.getEdad() > 11; });	

	}
	
	public static void filtrar(ArrayList<Persona> personas, Predicate<Persona> filtro)
	{
		for(Persona persona: personas) if (filtro.test(persona)==true)
				persona.mostrarNombre();
	}
	
	public static void filtrar2(ArrayList<Persona> personas, Predicate<Persona> filtro)
	{
		Consumer<Persona> consumidor = persona -> System.out.println(persona.getNombre());

		for(Persona persona: personas) 
			if (filtro.test(persona)==true)
			{
				consumidor.accept(persona);
			}	
	}
}
