import java.util.ArrayList;
import java.util.function.Function;

public class Funcion 
{

	public static void main(String[] args) 
	{
		Function<Persona, String> funcion = new Function<Persona, String>()
		{
			@Override
			public String apply(Persona p) 
			{
				return p.getNombre().toUpperCase();
			}
		};
		
		Persona persona = new Persona("Ana", 10, 50);
		System.out.println(funcion.apply(persona));	
		
		ArrayList<Persona> personas = new ArrayList<Persona>() ;
		
		personas.add(new Persona("Ana",10, 50));
		personas.add(new Persona("Mia",11, 50));		
		personas.add(new Persona("Paula",12, 60));
		personas.add(new Persona("Manuel",28, 40));
		personas.add(new Persona("Sofia",22, 30));
		
		calcular(personas, p -> p.getEdad()/2);
		
		int resultado = sumar(personas, p->p.getEdad() );
		System.out.println("suma edad " + resultado);
		
		int resultado2 = sumar(personas, p->p.getPeso() );
		System.out.println("suma peso " + resultado2);
		
		ArrayList<String> nombres = new ArrayList<String>();
		nombres.add("paula");
		nombres.add("ana");
		
		//Operator interfaces especial caso de function que recibe y devuelve el mismo tipo 
		//de valor.  
		nombres.replaceAll(nombre -> nombre.toUpperCase());
		System.out.println(nombres);
	
	}

	public static void calcular(ArrayList<Persona> personas, Function <Persona, Integer> funcion)
	{
		for(Persona persona: personas)
			System.out.println(persona.getNombre() + " " + funcion.apply(persona));
	}
	
	public static int sumar(ArrayList<Persona> personas, Function<Persona, Integer> funcion)
	{
		int ret = 0;
		for (Persona persona: personas)
			ret = ret + funcion.apply(persona);
		return ret;
	}
}
